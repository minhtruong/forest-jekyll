$(function () {
    var cloudName = "forestcommunitychurch";
    var gallery = $("#gallery");
    var tag = $("#meta-data").data("tag");
    var cl = cloudinary.Cloudinary.new({
        cloud_name: cloudName
    });
    var url = cl.url(tag + '.json', {
        type: "list"
    });
    $.get(url, function (data) {
        for (var i = 0; i < data.resources.length; i++) {
            var img = data.resources[i];
            var url = 'http://res.cloudinary.com/' + cloudName + '/image/upload/h_1000/' + img.public_id + '.' + img.format;
            var thumbUrl = 'http://res.cloudinary.com/' + cloudName + '/image/upload/h_400/' + img.public_id + '.' + img.format;
            gallery.append(
                '<div class="col-md-3 col-sm-4 col-xs-12">' +
                    '<a href="' + url + '" data-lightbox="group">' +
                        '<div class="gallery-thumb" style="background-image:url(\'' + thumbUrl + '\')"></div>' +
                    '</a><' +
                '/div>'
            );
        }
    });
});